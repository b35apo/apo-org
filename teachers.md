  
* Jan Kaisrlík – Avast, low level Linux drivers, komity do mainline
* Karel Kočí – CZNIC, vývoj open-source Turis routeru, správa systému,
               update, jádra atd.
* František Vacek – vedoucí vývojového týmu Elektroline.cz, řízení dep
               po celém světě, návrh vlastní otevřené komunikační
               infrastruktury, kterou s týmen z firmy vyvíjí jako open-source
               https://github.com/silicon-heaven, je rozhodujícím technickým
               guru i pro embedded vývoj ve firmě
* Roman Bartosinski – spolumajitel firmy, která pro Evropskou vesmírnou
               agenturu přepracovává a rozšiřuje architekturu procesorů
               ve VHDL a zabývá se zpracování obrazu s využitím HW/FPGA
               pipeline
                  http://www.daiteq.com/en/research/references
* Rostislav Lisový – v současné době nezávislý konzultant a vývojář v oblasti
               Linuxového jádra, příspěvky do Linux mainline
* Jan Štefan – embedded vývoj, mnoho let programování a seřizování robotů
               ve firmách. Nyní pracuje v rodinou vlastněné firmě, rozšíření
               si znalostí a zkušeností o činnosti procesoru vidí jako
               užitečný i pro svojí praxi
* Martin Vajnar
* Jan Štefan
* Petr Martinec